package cr.ac.una.mejenga;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainOpciones extends AppCompatActivity {
    Button btnOpciones;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_opciones);

        btnOpciones = (Button) findViewById(R.id.buttonCrear);

        btnOpciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainOpciones.this, MainCrearMejenga.class);
                startActivity(intent);

            }
        });
    }
}
